%define _lto_cflags %{nil}

# upstream default is clang (to use gcc for large parts set to 0)
%define clang_build 0

%global major 91
# LTO - Enable in Release builds, but consider disabling for development as it increases compile time
%global build_with_lto    1
# Require tests to pass?
%global require_tests     0
# LTO is default since F33 and F32 package is backported as is, so no LTO there
# Big endian platforms
Name:           mozjs%{major}
Version:        91.6.0
Release:        3
Summary:        SpiderMonkey JavaScript library
License:        MPL-2.0
Group:          System/Libraries
URL:            https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey
Source0:        https://ftp.mozilla.org/pub/firefox/releases/%{version}esr/source/firefox-%{version}esr.source.tar.xz
Source1:        https://ftp.mozilla.org/pub/firefox/releases/%{version}esr/source/firefox-%{version}esr.source.tar.xz.asc
Source2:        https://ftp.mozilla.org/pub/firefox/releases/%{version}esr/KEY#/mozilla.keyring
# Known failures with system libicu
Source3:        known_failures.txt

# Patches from mozjs78, rebased for mozjs91:
Patch01:        fix-soname.patch
Patch02:        copy-headers.patch
Patch03:        tests-increase-timeout.patch
Patch09:        icu_sources_data.py-Decouple-from-Mozilla-build-system.patch
Patch10:        icu_sources_data-Write-command-output-to-our-stderr.patch
# Build fixes - https://hg.mozilla.org/mozilla-central/rev/ca36a6c4f8a4a0ddaa033fdbe20836d87bbfb873
Patch12:        emitter.patch
# Build fixes
Patch14:        init_patch.patch
# TODO: Check with mozilla for cause of these fails and re-enable spidermonkey compile time checks if needed
Patch15:        spidermonkey_checks_disable.patch
Patch16:        spidermonkey_support_loongarch64.patch

BuildRequires:  autoconf213 cargo clang-devel gcc gcc-c++ libtool perl-devel llvm llvm-devel nasm pkgconfig python3-devel python3-setuptools
BuildRequires:  python3-six readline-devel zip rust pkgconfig(icu-i18n) >= 67.1 pkgconfig(libffi) pkgconfig(nspr) pkgconfig(zlib) icu

%description
SpiderMonkey is the code-name for Mozilla Firefox's C++ implementation of
JavaScript. It is intended to be embedded in other applications
that provide host environments for JavaScript.

%package -n libmozjs-%{major}-0
Summary:        JavaScript's library
Group:          System/Libraries

%description -n libmozjs-%{major}-0
JavaScript is the Netscape-developed object scripting language used in millions
of web pages and server applications worldwide. Netscape's JavaScript is a
superset of the ECMA-262 Edition 3 (ECMAScript) standard scripting language,
with only mild differences from the published standard.

This package contains the JavaScript's library.

%package devel
Summary:        Development files and tools for %{name}
Group:          Development/Libraries/Other
Requires:       libmozjs-%{major}-0 = %{version}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -p1 -n firefox-%{version}

cp LICENSE js/src/

cp %{SOURCE3} js/src/

rm -rf ../../modules/zlib

%build
pushd js/src
%if 0%{?clang_build} == 0
export CC=gcc
export CXX=g++
%endif
%ifarch %arm %ix86
# Limit RAM usage during link
export LDFLAGS="${LDFLAGS} -Wl,--no-keep-memory -Wl,--reduce-memory-overheads"
%endif

# Workaround
# error: options `-C embed-bitcode=no` and `-C lto` are incompatible
# error: could not compile `jsrust`.
# https://github.com/japaric/cargo-call-stack/issues/25
export RUSTFLAGS="-C embed-bitcode"

%if 0%{?build_with_lto}
# https://github.com/ptomato/mozjs/commit/36bb7982b41e0ef9a65f7174252ab996cd6777bd
export CARGO_PROFILE_RELEASE_LTO=true
%endif

export CFLAGS="%{optflags}"
export CXXFLAGS="$CFLAGS"
export LINKFLAGS="%{?__global_ldflags}"
export PYTHON="python3"

autoconf-2.13
%configure \
	--with-system-icu --with-system-zlib --disable-tests --disable-strip --with-intl-api \
	--enable-readline --enable-shared-js --enable-optimize --disable-debug --enable-pie --disable-jemalloc
%if 0%{?big_endian}
echo "Generate big endian version of config/external/icu/data/icud67l.dat"
pushd ../..
  /usr/sbin/icupkg -tb config/external/icu/data/icudt67l.dat config/external/icu/data/icudt67b.dat
  rm -f config/external/icu/data/icudt*l.dat
popd
%endif
%make_build
popd
%install
pushd js/src
%make_install

# Fix permissions
chmod -x %{buildroot}%{_libdir}/pkgconfig/*.pc

# Avoid multilib conflicts
case `uname -i` in
  i386 | sparc )
    wordsize="32"
    ;;
  x86_64 | sparc64 )
    wordsize="64"
    ;;
  *)
    wordsize=""
    ;;
esac

if test -n "$wordsize"
then
  mv %{buildroot}%{_includedir}/mozjs-%{major}/js-config.h \
     %{buildroot}%{_includedir}/mozjs-%{major}/js-config-$wordsize.h

  cat >%{buildroot}%{_includedir}/mozjs-%{major}/js-config.h <<EOF
#ifndef JS_CONFIG_H_MULTILIB
#define JS_CONFIG_H_MULTILIB

#include <bits/wordsize.h>

#if __WORDSIZE == 32
# include "js-config-32.h"
#elif __WORDSIZE == 64
# include "js-config-64.h"
#else
# error "unexpected value for __WORDSIZE macro"
#endif

#endif
EOF

fi

# Remove unneeded files
rm %{buildroot}%{_bindir}/js%{major}-config
rm %{buildroot}%{_libdir}/libjs_static.ajs

# Rename library and create symlinks, following fix-soname.patch
mv %{buildroot}%{_libdir}/libmozjs-%{major}.so \
   %{buildroot}%{_libdir}/libmozjs-%{major}.so.0.0.0
ln -s libmozjs-%{major}.so.0.0.0 %{buildroot}%{_libdir}/libmozjs-%{major}.so.0
ln -s libmozjs-%{major}.so.0 %{buildroot}%{_libdir}/libmozjs-%{major}.so
popd
%check
pushd js/src
# Run SpiderMonkey tests
%if 0%{?require_tests}
PYTHONPATH=tests/lib python3 tests/jstests.py -d -s -t 1800 --exclude-file=known_failures.txt --no-progress --wpt=disabled ../../js/src/dist/bin/js%{major}
%else
PYTHONPATH=tests/lib python3 tests/jstests.py -d -s -t 1800 --exclude-file=known_failures.txt --no-progress --wpt=disabled ../../js/src/dist/bin/js%{major} || :
%endif

# Run basic JIT tests
%if 0%{?require_tests}
PYTHONPATH=tests/lib python3 jit-test/jit_test.py -s -t 1800 --no-progress ../../js/src/dist/bin/js%{major} basic
%else
PYTHONPATH=tests/lib python3 jit-test/jit_test.py -s -t 1800 --no-progress ../../js/src/dist/bin/js%{major} basic || :
%endif

popd

%post -n libmozjs-%{major}-0 -p /sbin/ldconfig
%postun -n libmozjs-%{major}-0 -p /sbin/ldconfig

%files
%doc js/src/README.html
%{_bindir}/js%{major}

%files -n libmozjs-%{major}-0
%license LICENSE
%{_libdir}/libmozjs-%{major}.so.0*

%files devel
%{_libdir}/libmozjs-%{major}.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/mozjs-%{major}/

%changelog
* Thu Dec 15 2022 liuyu <liuyu@loongson.cn> - 91.6.0-3
- support loongarch64 in spidermonkey

* Fri May 06 2022 wangkerong <wangkerong@h-partners.com> - 91.6.0-2
- remove %dist

* Wed Mar 16 2022 liuyumeng <liuyumeng5@h-partners.com> - 91.6.0-1
- update to mozjs91.6.0-1

* Sat Dec 04 2021 wangkerong <wangkerong@huawei.com> - 78.15.0-1
- update to 78.15.0

* Tue May 11 2021 zhanzhimin <zhanzhimin@huawei.com> - 78.4.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error caused by rust

* Thu Nov 05 2020 chengguipeng <chengguipeng1@huawei.com> - 78.4.0-1
- Package init

